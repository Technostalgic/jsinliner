﻿using System;
using System.IO;

namespace JsInliner
{
	class Program
	{
		public static string InPath = "";
		public static string OutPath = "";

		public static int BytesRead;
		public static int BytesWritten;

		/// <summary>
		/// entry point of the program
		/// </summary>
		/// <param name="args">the arguments passed from the command line or batch script</param>
		static void Main(params string[] args) {

			parseArgs(args);
			outputParams();

			execute();

			Console.ReadKey();
		}

		/// <summary>
		/// starts reading the input file and converting it to the output file
		/// </summary>
		static void execute() {

			FileStream inFS = new FileStream(InPath, FileMode.Open, FileAccess.Read);
			StreamReader reader = new StreamReader(inFS);

			string outDir = Path.GetDirectoryName(OutPath);
			if(!Directory.Exists(outDir)) {
				Directory.CreateDirectory(outDir);
			}

			FileStream outFS = new FileStream(OutPath, FileMode.Create, FileAccess.Write);
			StreamWriter writer = new StreamWriter(outFS);

			Directory.SetCurrentDirectory(Path.GetDirectoryName(InPath));

			string matchStart = "<script src=";
			string matchEnd = "></script>";

			int bufferSize = 1048576;
			int increment = bufferSize - Math.Max(matchStart.Length, matchEnd.Length);
			char[] buffer = new char[bufferSize];
			int streamIndex = 0;

			int usedChars;
			while((usedChars = reader.ReadBlock(buffer, 0, bufferSize)) > 0) {

				// create a string from the character buffer and find the index of matchStart
				string bufString = new string(buffer, 0, usedChars);
				int matchIndex = bufString.IndexOf(matchStart);

				BytesRead += Math.Min(usedChars, increment);

				// if there is a match for matchStart within the buffer
				while(matchIndex >= 0) {

					writer.Write(bufString.Substring(0, matchIndex));

					string scriptPath = bufString.Substring(matchIndex, bufString.Length - matchIndex);
					int matchEndIndex = scriptPath.IndexOf(matchEnd);

					// if there is a matchEnd in the buffer
					if(matchEndIndex >= 0) {
						// set the script path to a substring from the end of matchStart to the
						//	beginning of matchEnd
						scriptPath = scriptPath.Substring(
							matchStart.Length,
							matchEndIndex - matchStart.Length).Replace("\"", "");

						bufString = bufString.Substring(
							matchIndex + matchStart.Length + matchEnd.Length + 
							scriptPath.Length + 2);
						matchIndex = bufString.IndexOf(matchStart);
					}

					// if the matchEnd is outside the buffer, we will have to search past the 
					//	end of the buffer
					else {
						// TODO implement function to search for matchEnd past the buffer and set
						//	bufSub equal

						// set match index to -1 so we know there will not be another script tag
						//	in the rest of the buffer
						matchIndex = -1;
					}

					writeScriptToOutFile(writer, scriptPath);

					// TODO write buffer to output file from end of matchEnd to end of buffer

				}

				// write the rest of the buffer to the output file
				writer.Write(bufString.Substring(0, Math.Min(increment, bufString.Length)));
				BytesWritten += Math.Min(increment, bufString.Length);

				streamIndex += increment;

				// output the progress
				Console.WriteLine(
					"Bytes read / written: " + 
					BytesRead.ToString() + ", " + BytesWritten.ToString() + 
					" (" + (int)(BytesRead / (float)inFS.Length * 100) + "%)");
			}

			// close all the file streams since we're finished with them
			writer.Close();
			outFS.Close();
			reader.Close();
			inFS.Close();
		}

		/// <summary>
		/// converts a script file to html inline script in the output file through the output filestream
		/// </summary>
		/// <param name="outWriter"></param>
		/// <param name="scriptPath"></param>
		static void writeScriptToOutFile(StreamWriter outWriter, string scriptPath) {

			FileStream scriptFS = new FileStream(scriptPath, FileMode.Open, FileAccess.Read);
			StreamReader scriptReader = new StreamReader(scriptFS);

			int bufferSize = 1024;
			char[] buffer = new char[bufferSize];
			int streamIndex = 0;

			outWriter.Write("<script>\n");

			int charsUsed;
			while((charsUsed = scriptReader.ReadBlock(buffer, 0, bufferSize)) > 0) {

				outWriter.Write(new string(buffer, 0, charsUsed));
				streamIndex += charsUsed;

				Console.WriteLine("Writing script bytes: " 
					+ streamIndex + "/" + scriptFS.Length + 
					" (" + (int)(streamIndex / (float)scriptFS.Length * 100) + "%)");
			}

			outWriter.Write("</script>\n");

			scriptReader.Close();
			scriptFS.Close();
		}

		/// <summary>
		/// parse the arguments passed by the user
		/// </summary>
		/// <param name="args">the array of arguments passed</param>
		static void parseArgs(params string[] args) {

			string inPath = null;
			string outPath = null;

			for(int i = 0; i < args.Length; i++) {
				string arg = args[i];
				switch(i) {
					case 0:
						inPath = arg;
						break;
					case 1:
						outPath = arg;
						break;
				}
			}

			// if the user specifies a directory for input, find an html file in that directory
			if(inPath != null) {
				if(string.IsNullOrEmpty(Path.GetFileName(inPath))) {
					inPath = getInputTarget(inPath);
				}
			}

			InPath = inPath ?? getInputTarget(Directory.GetCurrentDirectory());
			OutPath = outPath ??
				Path.Combine(Directory.GetCurrentDirectory(), "build", Path.GetFileName(InPath));
		}

		/// <summary>
		/// Outputs all the parameters set for program execution
		/// </summary>
		static void outputParams() {
			Console.WriteLine("In:\t" + InPath);
			Console.WriteLine("Out:\t" + OutPath);
		}

		/// <summary>
		/// gets the input target from a supplied directory
		/// </summary>
		/// <param name="inDir">the directory that contains the target HTML file</param>
		static string getInputTarget(string inDir) {

			string[] files = Directory.GetFiles(inDir, "*.html");
			
			if(files.Length <= 0) {
				throw new Exception("No valid HTML file found");
			}

			for(int i = files.Length - 1; i>= 0; i--) {
				if(Path.GetFileNameWithoutExtension(files[i]).ToLower() == "index")
					return files[i];
			}

			return files[0];
		}
	}
}
