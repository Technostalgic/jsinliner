/**
 * @abstract
 */
class CollisionShape{

	/** @type {CollisionShape} */
	constructor(){ }

	/** @abstract @type {Vec2} the geometric center of the shape*/
	get centroid(){ }
	/** @abstract @param {Vec2} value */
	set centroid(value){ }

	/** @type {Rect} the bounding box that completely encapsulates the shape*/
	get boundingBox(){ }

	/** 
	 * @abstract @type {CircleCastResult} casts a ray to test for collision with the collision shape
	 * @param {Vec2} rayStart the start point of the ray
	 * @param {Vec2} rayEnd the end point of the ray
	*/
	rayCast(rayStart, rayEnd){ }

	/** 
	 * @abstract @type {CircleCastResult} casts a ray with a non-zero width to test for collision 
	 * with the collision shape
	 * @param {Vec2} rayStart the start point of the ray
	 * @param {Vec2} rayEnd the end point of the ray
	 * @param {Number} radius the width of the ray to cast
	*/
	circleCast(rayStart, rayEnd, radius){ }

	/**
	 * @abstract
	 * @param {CanvasRenderingContext2D} ctx the context used to render the shape
	 * @param {String} strokeStyle the style that will be used when rendering the outline
	 */
	drawOutline(ctx, strokeStyle, lineWidth = 1){ }
}

/**
 * a data structure that represents an axis-aligned bounding box
 */
class Rect extends CollisionShape{

	/**
	 * @type {Rect}
	 * @param {Vec2} position the position of the top left of the rectangle
	 * @param {Vec2} size the width and height of the rectangle
	 */
	constructor(position, size){
		super();

		/** @type {Vec2} */
		this.position = position;

		/** @type {Vec2} */
		this.size = size;
	}

	/**@type {Rect}*/
	static FromSides(left, top, right, bottom) {
			return new Rect(new Vec2(left, top), new Vec2(right - left, bottom - top));
	}

	/**
		 * @type {Rect} creates an axis aligned rectangle from two points
		 * @param {Vec2} pointA
		 * @param {Vec2} pointB
		*/
	static FromPoints(pointA, pointB){
	
			// define the min and max boundaries
			let minX = Math.min(pointA.x, pointB.x);
			let minY = Math.min(pointA.y, pointB.y);
			let maxX = Math.max(pointA.x, pointB.x);
			let maxY = Math.max(pointA.y, pointB.y);
	
			// return a rectangle from the calculated sides
			return Rect.FromSides(minX, maxX, minY, maxY);
	}

	/**
		 * @type {Rect} creates a rectangle that completely overlaps a circle with the specified 
		 * radius and center
		 * @param {Number} radius the radius of the circle
		 * @param {Vec2} center the position of the center of the circle
		 */
	static FromRadius(radius, center){
			
			// create a new rect
			let r = new Rect();
	
			// set it's bounds based on the circle position and radius
			r.left = center.x - radius;
			r.right = center.x + radius;
			r.top = center.y - radius;
			r.bottom = center.y + radius;
	
			return r;
	}
	
	/**
	 * @type {Rect} Creates a new rectangle that overlaps both the specified rectangles
	 * @param {Rect} rectA 
	 * @param {Rect} rectB 
	 */
	static OverlapRect(rectA, rectB){

		// find the top left of the overlap rect by getting the min x and y values of rect A & B
		let min = new Vec2(
			Math.min(rectA.left, rectB.left), // the left-most x value of both rectangles
			Math.min(rectA.top, rectB.top) // the highest y value of both rectangles
		);

		// find the bottom right of the overlap rect by getting the min x and y of rect A & B
		let max = new Vec2(
			Math.max(rectA.right, rectB.right), // the right-most x value of both rectangles
			Math.max(rectA.bottom, rectB.bottom) // the lowest y value of both rectangles
		);

		// construct and return a new rectangle from the two points we found
		return Rect.FromPoints(min, max);
	}

	/**
	 * @type {Rect} returns the area of two intersecting rectangles, if they are not intersecting
	 * it will return null
	 * @param {Rect} rectA 
	 * @param {Rect} rectB 
	 */
	static IntersectRect(rectA, rectB){

		// find the right-most left edge of the two rects and the lowest top edge
		let min = new Vec2(
			Math.max(rectA.left, rectB.left),
			Math.max(rectA.top, rectB.top)
		);

		// find the left-most right edge of the two rects and the highest bottom edge
		let max = new Vec2(
			Math.min(rectA.right, rectb.right),
			Math.min(rectA.bottom, rectB.bottom)
		);

		// determine the rectangle's size from the max and min points
		let size = max.minus(min);

		// if the rectangle has negative or zero area, return null
		if(size.x <= 0 || size.y <= 0) return null;

		// return new rect constructed from the min (top left) and computed size
		return new Rect(min, size);
	}

	/** @type {Vec2} */
	get topLeft(){
		return this.position.clone;
	}
	/** @type {Vec2} */
	get topRight(){
		return new Vec2(this.position.x, this.position.y + this.size.y);
	}
	/** @type {Vec2} */
	get bottomLeft(){
		return new Vec2(this.position.x, this.position.y + this.size.y)
	}
	/** @type {Vec2} */
	get bottomRight(){
		return this.position.plus(this.size);
	}
	/** @type {Vec2} */
	get center(){
		return this.position.plus(this.size.scale(0.5));
	}
	/** @param {Vec2} value */
	set center(value){
		this.position = value.plus(this.size.scale(-0.5));
	}

	/** @type {Vec2} */
	get left(){ return this.position.x; }
	/** @type {Vec2} */
	get right(){ return this.position.x + this.size.x; }
	/** @type {Vec2} */
	get top() { return this.position.y; }
	/** @type {Vec2} */
	get bottom(){ return this.position.y + this.size.y; }
	
	/**
	 * @type {Rect} expands the edges of the rectangle outwards from the center by the specified 
	 * size and returns the result
	 * @param {Vec2} size 
	 */
	expand(size){
		return new Rect(
			this.position.minus(new Vec2(size)), 
			this.size.plus(new Vec2(2 * size))
			);
	}

	/**
	 * @type {RaycastHit} casts a ray against the specified rectangle and if hit, returns
	 * information about the raycast hit, otherwise returns null 
	 * (algorithm adapted from "zacharmarz" on gamedev.stackexchange.com)
	 * @param {Vec2} rayStart the origin of the ray
	 * @param {Vec2} rayEnd the vector direction that the ray is pointing in
	 */
	rayCast(ray) {

		// define the t values
		let t1 = (this.left - ray.rayStart.x) * ray.dirfrac.x || -1;
		let t2 = (this.right - ray.rayStart.x) * ray.dirfrac.x || -1;
		let t3 = (this.top - ray.rayStart.y) * ray.dirfrac.y || -1;
		let t4 = (this.bottom - ray.rayStart.y) * ray.dirfrac.y || -1;

		// find t min/max values
		let tmin = Math.max(Math.min(t1, t2), Math.min(t3, t4));
		let tmax = Math.min(Math.max(t1, t2), Math.max(t3, t4));

		// if tmin > tmax, ray doesn't intersect AABB
		if (tmin > tmax) {
			return RaycastResult.Invalid;
		}

		// define and return the raycast hit information
		let r = new RaycastResult_rect(ray, tmin, tmax, t1, t2, t3, t4);
		return r;
	}

	/**
	 * @override @type {CirclecastResult}
	 * @param {Ray} ray 
	 * @param {Number} radius 
	 */
	circleCast(ray, radius){
		
		// define the t values
		let t1 = (this.left - radius - ray.rayStart.x) * ray.dirfrac.x || -1;
		let t2 = (this.right + radius - ray.rayStart.x) * ray.dirfrac.x || -1;
		let t3 = (this.top - radius - ray.rayStart.y) * ray.dirfrac.y || -1;
		let t4 = (this.bottom + radius - ray.rayStart.y) * ray.dirfrac.y || -1;

		// find t min/max values
		let tmin = Math.max(Math.min(t1, t2), Math.min(t3, t4));
		let tmax = Math.min(Math.max(t1, t2), Math.max(t3, t4));

		// if tmin > tmax, ray doesn't intersect AABB
		if (tmin > tmax) {
			return RaycastResult.Invalid;
		}

		// define and return the raycast hit information
		let r = new CirclecastResult_rect(ray, radius, tmin, tmax, t1, t2, t3, t4, this);
		return r;
	}

	/**
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {String} strokeStyle 
	 * @param {Number} lineWidth 
	 */
	drawOutline(ctx, strokeStyle, lineWidth = 1){

		ctx.strokeStyle = strokeStyle;
		ctx.lineWidth = lineWidth;

		ctx.beginPath();
		ctx.moveTo(this.left, this.top);
		ctx.lineTo(this.right, this.top);
		ctx.lineTo(this.right, this.bottom);
		ctx.lineTo(this.left, this.bottom);
		ctx.closePath();
		ctx.stroke();
	}
}

/**
 * a data structure that represents a simple circle, that is a point in space and a radius
 */
class Circle extends CollisionShape{

	/**
	 * @type {Circle}
	 * @param {Vec2} center the position of the center of the circle
	 * @param {Number} radius the circle's radius
	 */
	constructor(center, radius){
		super();
		this.center = center;
		this.radius = radius;
	}
	
	/**
	 * @override @type {RaycastResult}
	 * @param {Ray} ray the ray being cast
	 */
	rayCast(ray){
		
		// offset the line to be relative to the circle
		let rayStart = ray.rayStart.minus(this.center);
		let rayEnd = ray.rayEnd.minus(this.center);
		
		// calculate the dot products between the origin and target vectors
		let aoa = rayStart.dot(rayStart);
		let aob = rayStart.dot(rayEnd);
		let bob = rayEnd.dot(rayEnd);

		// calculate the ratios used to find the determinant and the t-values
		let qa = aoa - 2.0 * aob + bob;
		let qb = -2.0 * aoa + 2.0 * aob;
		let qc = aoa - this.radius * this.radius;

		// caclulate the determinant used to determine if there was an intersection or not
		let determinant = qb * qb - 4.0 * qa * qc;
		
		// if the determinant is negative, the ray completely misses
		if(determinant >= 0.0){
			 
			// compute the nearest of the two t values
			let t1 = (-qb - Math.sqrt(determinant)) / (2.0 * qa);

			// compute the furthest of the t values
			let t2 = (-qb + Math.sqrt(determinant)) / (2.0 * qa);

			return new RaycastResult_circle(ray, this.center, t1, t2);
		}
		else{
			return RaycastResult_circle.Invalid;
		}
	}

	/**
	 * @override @type {CircleCastResult}
	 * @param {Vec2} rayStart the start point of the ray
	 * @param {Vec2} rayEnd the end point of the ray
	 * @param {Number} radius the width of the ray to cast
	 */
	circleCast(ray, radius){
		
		// offset the line to be relative to the circle
		let a = ray.rayStart.minus(this.center);
		let b = ray.rayEnd.minus(this.center);
		
		// add the ray radius to the circle radius to get the total radius
		let tRadius = this.radius + radius;
		
		// calculate the dot products between the origin and target vectors
		let aoa = a.dot(a);
		let aob = a.dot(b);
		let bob = b.dot(b);

		// calculate the ratios used to find the determinant and the t-values
		let qa = aoa - 2.0 * aob + bob;
		let qb = -2.0 * aoa + 2.0 * aob;
		let qc = aoa - tRadius * tRadius;

		// caclulate the determinant used to determine if there was an intersection or not
		let determinant = qb * qb - 4.0 * qa * qc;
		
		// if the determinant is negative, the ray completely misses
		let r = null;
		if(determinant >= 0.0){
			 
			// compute the nearest of the two t values
			let t1 = (-qb - Math.sqrt(determinant)) / (2.0 * qa);

			// compute the furthest of the t values
			let t2 = (-qb + Math.sqrt(determinant)) / (2.0 * qa);

			return new CirclecastResult_circle(ray, radius, this.center, t1, t2);
		}
		else{
			return CirclecastResult.Invalid;
		}
	}
	
	/**
	 * @override
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {String} strokestyle 
	 * @param {Number} lineWidth 
	 */
	drawOutline(ctx, strokestyle, lineWidth = 1){
		ctx.strokeStyle = strokestyle;
		ctx.lineWidth = lineWidth;
		ctx.beginPath();
		ctx.arc(
			this.center.x,
			this.center.y,
			this.radius,
			0, Math.PI * 2 );
		ctx.closePath();
		ctx.stroke();
	}
}