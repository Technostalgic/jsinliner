
/**
 * A data structure that represents a 2-dimensional vector, with x and y components.
 */
class Vec2{
	
	/**
	 * @type {Vec2} constructs a new vector object
	 * @param {Number} x the x-component of the vector
	 * @param {Number} y the y-component of the vector
	 */
	constructor(x = 0, y = x){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * @type {Vec2} constructs a vector from a specified direction and (optional)
	 * @param {Number} direction the direction, in radians, that the vector will point
	 * @param {Number} magnitude the magnitude of the vector, 1 if not specified
	 */
	static FromDirection(direction, magnitude = 1){
		return new Vec2(Math.cos(direction) * magnitude, Math.sin(direction) * magnitude);
	}
	
	/**
	 * @type {Vec2} returns a new vector that is linearly interpolated from two specified vectors
	 * @param {Number} a the vector to interpolate from
	 * @param {Number} b the vector to interpolate to
	 * @param {Number} delta the amount of progress that has been made from a to b, from 0 to 1
	 */
	static Lerp(a, b, delta = 0.5){
		let dif = b.minus(a);
		return a.plus(dif.scale(delta));
	}

	/** @type {Vec2} returns a vector that points left */
	static get Left(){ return new Vec2(-1, 0); }
	/** @type {Vec2} returns a vector that points Right */
	static get Right(){ return new Vec2(1, 0); }
	/** @type {Vec2} returns a vector that points Up */
	static get Up(){ return new Vec2(0, -1); }
	/** @type {Vec2} returns a vector that points Down */
	static get Down(){ return new Vec2(0, 1); }
	
	/**
	 * @type {Number} calculates and returns the dot product between this and another vector
	 * @param {Vec2} vec the vector to apply the operation with respect to
	 */
	dot(vec){
		return this.x * vec.x + this.y * vec.y;
	}
	
	/**
	 * @type {Vec2} returns the sum of this and another vector
	 * @param {Number} vec the vector to add
	 */
	plus(vec){
		return new Vec2(this.x + vec.x, this.y + vec.y);
	}

	/**
	 * @type {Vec2} returns the sum of this and another vector's negation
	 * @param {Number} vec the vector to subtract from this
	 */
	minus(vec){
		return this.plus(vec.inverted);
	}

	/**
	 * @type {Vec2} returns a vector that is equivelant to this vector scaled by a numerical value
	 * @param {Number} factor the amount to scale this vector by
	 */
	scale(factor){
		return new Vec2(this.x * factor, this.y * factor)
	}
	
	/** @type {Vec2} the negation of this vector */
	get inverted(){
		return this.scale(-1);
	}
	
	/** @type {Vec2} the normalized result of this vector */
	get normalized(){
		return this.scale(1 / this.magnitude);
	}
	
	/** @type {Vec2} the magnitude, or length, of this vector */
	get magnitude(){
		return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))
	}
	
	/** @type {Vec2} the direction that this vector points, in radians */
	get direction(){
		return Math.atan2(this.y, this.x);
	}

	/** @type {Vec2} new instance of a vector with the same values as this one */
	get clone(){
		return new Vec2(this.x, this.y);
	}
	
	/**
	 * draws a point that represents the translation value of this vector
	 * @param {CanvasRenderingContext2D} ctx the canvas context that is used to render
	 * @param {String} fillStyle the fillstyle of the point that is rendered
	 * @param {Number} size the width and height of the point to be drawn, in pixels
	 */
	draw(ctx, fillStyle, size = 2){
		ctx.fillStyle = fillStyle;
		ctx.fillRect(
			this.x - size / 2, 
			this.y - size / 2, 
			size, size);
	}
}