/**
 * a data structure that contains calculated information about a ray that starts and ends at a 
 * specified point
 */
class Ray{

	/**
	 * @type {Ray}
	 * @param {Ve2} start 
	 * @param {Ve2} end 
	 */
	constructor(start, end){

		/** @type {Vec2} */
		this._rayStart = start;
		/** @type {Vec2} */
		this._rayEnd = end;

		/** @type {Number} */
		this._magnitude;
		/** @type {Vec2} */
		this._dif;
		/** @type {Vec2} */
		this._dirNormal;
		/** @type {Vec2} */
		this._dirFrac;
	}

	/** @type {Vec2} */
	get rayStart(){
		return this._rayStart;
	}

	/** @type {Vec2} */
	get rayEnd(){
		return this._rayEnd;
	}

	/** @type {Vec2} the vector from the start of the ray to the end of the ray */
	get dif(){
		if(this._dif == null)
			this._dif = this.rayEnd.minus(this.rayStart);
		return this._dif;
	}

	/** @type {Number} the length of the ray from start to end*/
	get magnitude(){
		if(this._magnitude == null)
			this._magnitude = this.dif.magnitude;
		return this._magnitude;
	}

	/** @type {Vec2} the normalized vector that points in the same direction as the ray */
	get dirNormal(){
		if(this._dirNormal == null)
			this._dirNormal = this.dif.normalized;
		return this._dirNormal;
	}

	/** @type {Vec2} the multplicative inverse of the direction normal */
	get dirfrac(){
		if(this._dirFrac == null)
			this._dirFrac = new Vec2(1 / this.dirNormal.x, 1 / this.dirNormal.y);
		return this._dirFrac;
	}
}

/**
 * @abstract
 */
class RaycastResult{

	/**
	 * @type {RaycastResult}
	 * @param {Vec2} rayStart 
	 * @param {Vec2} rayEnd 
	 */
	constructor(ray){ 
		/** @type {Boolean} */
		this.isValid = true;
		/** @type {Ray} */
		this.ray = ray;
	}

	/** @type {RaycastResult} */
	static get Invalid(){
		let r = new RaycastResult();
		r.isValid = false;
		return r;
	}

	/** @abstract @type {Boolean} */
	get didEnter(){ }
	/** @abstract @type {Boolean} */
	get didExit(){ }

	/** @abstract @type {Vec2} */
	get entryNormal(){ }
	/** @abstract @type {Vec2} */
	get exitNormal(){ }

	/** @abstract @type {Vec2} */
	get entryPoint(){ }
	/** @abstract @type {Vec2} */
	get exitPoint(){ }
}

/**
 * a data structure to explicitly set raycast results
 */
class RaycastResult_explicit extends RaycastResult{

	/**
	 * @type {RaycastResult_explicit}
	 * @param {Vec2} rayStart 
	 * @param {Vec2} rayEnd 
	 * @param {Vec2} entryNormal 
	 * @param {Vec2} exitNormal 
	 * @param {Vec2} entryPoint 
	 * @param {Vec2} exitPoint 
	 */
	constructor(ray, entryNormal, exitNormal, entryPoint, exitPoint){
		super(ray);

		let dif = rayEnd.minus(rayStart);
		let mag = dif.magnitude;
		let entryOff = entryPoint.minus(rayStart);
		let exitOff = exitPoint.minus(rayStart);

		/** @type {Boolean} */
		this._didEnter = (
			mag >= entryOff.magnitude && 
			Math.sign(dif.x) == Math.sign(entryOff.x) &&
			Math.sign(dif.y) == Math.sign(entryOff.y) );

		/** @type {Boolean} */
		this._didExit = ( 
			mag >= exitOff.magnitude && 
			Math.sign(dif.x) == Math.sign(exitOff.x) &&
			Math.sign(dif.y) == Math.sign(exitOff.y) );

		/** @type {Vec2} */
		this._entryNormal = entryNormal;
		/** @type {Vec2} */
		this._exitNormal = exitNormal;
		/** @type {Vec2} */
		this._entryPoint = entryPoint;
		/** @type {Vec2} */
		this._exitPoint = exitPoint;
	}
	
	/** @abstract @type {Boolean} */
	get didEnter(){ return this._didEnter; }
	/** @abstract @type {Boolean} */
	get didExit(){ return this._didExit; }

	/** @abstract @type {Vec2} */
	get entryNormal(){ return this._entryNormal; }
	/** @abstract @type {Vec2} */
	get exitNormal(){ return this._exitNormal; }

	/** @abstract @type {Vec2} */
	get entryPoint(){ return this._entryPoint; }
	/** @abstract @type {Vec2} */
	get exitPoint(){ return this._exitPoint; }
}

/**
 * a data structure that holds information about a raycast intersection with a circle
 */
class RaycastResult_rect extends RaycastResult{

	/**
	 * @type {RaycastResult_rect}
	 * @param {Ray} ray 
	 * @param {Number} rayLength 
	 * @param {Number} t1 the t value for the left side of the rect
	 * @param {Number} t2 the t value for the right side of the rect
	 * @param {Number} t3 the t value for the top side of the rect
	 * @param {Number} t4 the t value for the bottom side of the rect
	 */
	constructor(ray, tmin, tmax, t1, t2, t3, t4){
		super(ray);

		/** @type {Number} */
		this.tmin = tmin;
		/** @type {Number} */
		this.tmax = tmax;
		/** @type {Number} */
		this.t1 = t1;
		/** @type {Number} */
		this.t2 = t2;
		/** @type {Number} */
		this.t3 = t3;
		/** @type {Number} */
		this.t4 = t4;

		/** @type {Boolean} */
		this._didEnter = null;
		/** @type {Boolean} */
		this._didExit = null;
		/** @type {Vec2} */
		this._entryNormal = null;
		/** @type {Vec2} */
		this._exitNormal = null;
		/** @type {Vec2} */
		this._entryPoint = null;
		/** @type {Vec2} */
		this._exitPoint = null;
	}

	/** @type {Boolean} */
	get didEnter(){
		if(this._didEnter == null)
			this._didEnter = this.tmin >= 0 && this.tmin <= this.ray.magnitude;
		return this._didEnter;
	}

	/** @type {Boolean} */
	get didExit(){
		if(this._didExit == null)
			this._didExit = this.tmax >= 0 && this.tmax <= this.ray.magnitude;
		return this._didExit;
	}

	/** @type {Vec2} */
	get entryPoint(){
		if(this._entryPoint == null)
			this._entryPoint = this.ray.rayStart.plus(this.ray.dirNormal.scale(this.tmin));
		return this._entryPoint;
	}

	/** @type {Vec2} */
	get exitPoint(){
		if(this._exitPoint == null)
			this._exitPoint = this.ray.rayStart.plus(this.ray.dirNormal.scale(this.tmax));
		return this._exitPoint;
	}

	/** @type {Vec2} */
	get entryNormal(){
		if(this._entryNormal == null)
			this._calculateNormals();
		return this._entryNormal;
	}

	/** @type {Vec2} */
	get exitNormal(){
		if(this._exitNormal == null)
			this._calculateNormals();
		return this._exitNormal;
	}

	_calculateNormals(){
		
		// if the entry or exit point is on the left side, set the entry/exit normal accordingly
		if(this.tmin == this.t1) this._entryNormal = Vec2.Left;
		else if (this.tmax == this.t1) this._exitNormal = Vec2.Left;

		// if the entry or exit point is on the right side, set the entry/exit normal accordingly
		if(this.tmin == this.t2) this._entryNormal = Vec2.Right;
		else if (this.tmax == this.t2) this._exitNormal = Vec2.Right;

		// if the entry or exit point is on the top side, set the entry/exit normal accordingly
		if(this.tmin == this.t3) this._entryNormal = Vec2.Up;
		else if (this.tmax == this.t3) this._exitNormal = Vec2.Up;

		// if the entry or exit point is on the bottom side, set the entry/exit normal accordingly
		if(this.tmin == this.t4) this._entryNormal = Vec2.Down;
		else if (this.tmax == this.t4) this._exitNormal = Vec2.Down;
	}
}

/**
 * a data structure that holds information about a raycast intersection with a circle
 */
class RaycastResult_circle extends RaycastResult{

	/**
	 * @type {RaycastResult_circle}
	 * @param {Ray} ray 
	 * @param {Vec2} circlePos 
	 * @param {Number} tmin 
	 * @param {Number} tmax 
	 */
	constructor(ray, circlePos, tmin, tmax){
		super(ray);
		
		/** @type {Vec2} */
		this.circlePos = circlePos;
		/** @type {Number} */
		this.tmin = tmin;
		/** @type {Number} */
		this.tmax = tmax;

		// private values will be calculated only when/if their corresponding properties 
		// are requested
		/** @private @type {Vec2} */
		this._entryNormal = null;
		/** @private @type {Vec2} */
		this._exitNormal = null;
		/** @private @type {Vec2} */
		this._entryPoint = null;
		/** @private @type {Vec2} */
		this._exitPoint = null;
	}
	
	/** @override @type {Boolean} */
	get didEnter(){
		return this.isValid && 0.0 <= this.tmin && this.tmin <= 1.0;
	}
	/** @override @type {boolean} */
	get didExit(){
		return this.isValid && 0.0 <= this.tmax && this.tmax <= 1.0;
	}
	
	/** @override @type {Vec2} */
	get entryNormal(){
		if(this._entryNormal == null)
			this._entryNormal = this.circlePos.minus(Vec2.Lerp(this.ray.rayStart, this.ray.rayEnd, this.tmin)).normalized.inverted;
		return this._entryNormal;
	}
	/** @override @type {Vec2} */
	get exitNormal(){
		if(this._exitNormal == null)
			this._exitNormal = this.circlePos.minus(Vec2.Lerp(this.ray.rayStart, this.ray.rayEnd, this.tmax)).normalized.inverted;
		return this._exitNormal;
	}
	
	/** @override @type {Vec2} */
	get entryPoint(){
		if(this._entryPoint == null)
			this._entryPoint = Vec2.Lerp(this.ray.rayStart, this.ray.rayEnd, this.tmin);
		return this._entryPoint;
	}
	/** @override @type {Vec2} */
	get exitPoint(){
		if(this._exitPoint == null)
			this._exitPoint = Vec2.Lerp(this.ray.rayStart, this.ray.rayEnd, this.tmax);
		return this._exitPoint;
	}
}

/**
 * @abstract
 */
class CirclecastResult extends RaycastResult{
	
	/**
	 * @type {CirclecastResult}
	 * @param {Ray} ray
	 * @param {Number} rayWidth 
	 */
	constructor(ray, rayWidth){
		super(ray);
		
		/** @type {Number} */
		this.rayWidth = rayWidth;
	}
	
	/** @abstract @type {Vec2} */
	get firstContact(){ }
	/** @abstract @type {Vec2} */
	get lastContact(){ }
}

/**
 * a data structure to hold information about a circlecast test against a rect
 */
class CirclecastResult_rect extends CirclecastResult{

	/**
	 * @type {RaycastResult_circle}
	 * @param {Ray} ray
	 * @param {Vec2} circlePos
	 * @param {Number} tmin
	 * @param {Number} tmax
	 */
	constructor(ray, rayWidth, tmin, tmax, t1, t2, t3, t4, rect){
		super(ray);
		
		/** @type {Boolean} */
		this._useSurrogateEntry = null;
		/** @type {Boolean} */
		this._useSurrogateExit = null;

		/** @type {CirclecastResult_circle} */
		this.surrogateEntryResult = null;
		/** @type {CirclecastResult_circle} */
		this.surrogateExitResult = null;

		/** @type {Number} */
		this.rayWidth = rayWidth;
		/** @type {Number} */
		this.tmin = tmin;
		/** @type {Number} */
		this.tmax = tmax;
		/** @type {Number} */
		this.t1 = t1;
		/** @type {Number} */
		this.t2 = t2;
		/** @type {Number} */
		this.t3 = t3;
		/** @type {Number} */
		this.t4 = t4;
		/** @type {Rect} */
		this.rect = rect;

		// private values will be calculated only when/if their corresponding properties 
		// are requested
		/** @private @type {Vec2} */
		this._entryNormal = null;
		/** @private @type {Vec2} */
		this._exitNormal = null;
		/** @private @type {Vec2} */
		this._entryPoint = null;
		/** @private @type {Vec2} */
		this._exitPoint = null;
		/** @private @type {Vec2} */
		this._firstContact = null;
		/** @private @type {Vec2} */
		this._lastContact = null;
	}
	
	/** @type {Boolean} */
	get didEnter(){
		if(this.useSurrogateEntry)
			return this.surrogateEntryResult.didEnter;
		if(this._didEnter == null)
			this._didEnter = this.tmin >= 0 && this.tmin <= this.ray.magnitude;
		return this._didEnter;
	}
	/** @type {Boolean} */
	get didExit(){
		if(this.useSurrogateExit)
			return this.surrogateExitResult.didExit;
		if(this._didExit == null)
			this._didExit = this.tmax >= 0 && this.tmax <= this.ray.magnitude;
		return this._didExit;
	}

	/** @type {Vec2} */
	get entryPoint(){
		if(this.useSurrogateEntry)
			return this.surrogateEntryResult.entryPoint;
		return this._selfEntryPoint;
	}
	/** @type {Vec2} */
	get exitPoint(){
		if(this.useSurrogateExit)
			return this.surrogateExitResult.exitPoint;
		return this._selfExitPoint;
	}

	/** @type {Boolean} */
	get useSurrogateEntry(){
		if(this._useSurrogateEntry == null)
			;
		return this._useSurrogateEntry;
	}
	/** @type {Boolean} */
	get useSurrogateExit(){
		if(this._useSurrogateExit == null)
			;
		return this._useSurrogateExit;
	}

	/** @type {Boolean} */
	get useSurrogateEntry(){
		if(this._useSurrogateEntry == null)
			this._calcSurrogateEntry();
		return this._useSurrogateEntry;
	}
	/** @type {Boolean} */
	get useSurrogateExit(){
		if(this._useSurrogateExit == null)
			this._calcSurrogateExit();
		return this._useSurrogateExit;
	}

	/** @type {Vec2} */
	get _selfEntryPoint(){
		if(this._entryPoint == null)
			this._entryPoint = this.ray.rayStart.plus(this.ray.dirNormal.scale(this.tmin));
		return this._entryPoint;
	}
	/** @type {Vec2} */
	get _selfExitPoint(){
		if(this._exitPoint == null)
			this._exitPoint = this.ray.rayStart.plus(this.ray.dirNormal.scale(this.tmax));
		return this._exitPoint;
	}

	/** @type {Vec2} */
	get entryNormal(){
		if(this.useSurrogateEntry)
			return this.surrogateEntryResult.entryNormal;
		if(this._entryNormal == null)
			this._calculateNormals();
		return this._entryNormal;
	}

	/** @type {Vec2} */
	get exitNormal(){
		if(this.useSurrogateExit)
			return this.surrogateExitResult.exitNormal;
		if(this._exitNormal == null)
			this._calculateNormals();
		return this._exitNormal;
	}

	/** @override @type {Vec2} */
	get firstContact(){
		if(this.useSurrogateEntry)
			return this.surrogateEntryResult.firstContact;
		if(this._firstContact == null)
			this._firstContact = this.entryPoint.plus(this.entryNormal.scale(-circlecastWidth));
		return this._firstContact;
	}
	/** @override @type {Vec2} */
	get lastContact(){
		if(this.useSurrogateExit)
			return this.surrogateExitResult.lastContact;
		if(this._lastContact == null)
			this._lastContact = this.exitPoint.plus(this.exitNormal.scale(-circlecastWidth));
		return this._lastContact;
	}

	_calculateNormals(){
		
		// if the entry or exit point is on the left side, set the entry/exit normal accordingly
		if(this.tmin == this.t1) this._entryNormal = Vec2.Left;
		else if (this.tmax == this.t1) this._exitNormal = Vec2.Left;

		// if the entry or exit point is on the right side, set the entry/exit normal accordingly
		if(this.tmin == this.t2) this._entryNormal = Vec2.Right;
		else if (this.tmax == this.t2) this._exitNormal = Vec2.Right;

		// if the entry or exit point is on the top side, set the entry/exit normal accordingly
		if(this.tmin == this.t3) this._entryNormal = Vec2.Up;
		else if (this.tmax == this.t3) this._exitNormal = Vec2.Up;

		// if the entry or exit point is on the bottom side, set the entry/exit normal accordingly
		if(this.tmin == this.t4) this._entryNormal = Vec2.Down;
		else if (this.tmax == this.t4) this._exitNormal = Vec2.Down;
	}

	_getSurrogate(point){

		let toLeft = point.x < this.rect.left
		let toRight = point.x > this.rect.right;
		let above = point.y < this.rect.top;
		let below = point.y > this.rect.bottom;
		
		let x = null;
		let y = null;
		if(above){
			y = 0;
			if(toLeft)
				x = 0;
			else if (toRight)
				x = 1;
		}
		else if(below){
			y = 1;
			if(toLeft)
				x = 0;
			if(toRight)
				x = 1;
		}

		if(x == null || y == null){
			return null;
		}

		let circ = new Circle(
			new Vec2(
				this.rect.position.x + this.rect.size.x * x, 
				this.rect.position.y + this.rect.size.y * y), 
			0);
		let surrogate = circ.circleCast(this.ray, this.rayWidth);

		return surrogate;
	}
	_calcSurrogateEntry(){
		this.surrogateEntryResult = this._getSurrogate(this._selfEntryPoint);
		this._useSurrogateEntry = this.surrogateEntryResult != null;
	}
	_calcSurrogateExit(){
		this.surrogateExitResult = this._getSurrogate(this._selfExitPoint);
		this._useSurrogateExit = this.surrogateExitResult != null;
	}
}

/**
 * a data structure to hold information about a circlecast test against a circle
 */
class CirclecastResult_circle extends CirclecastResult{

	/**
	 * @type {CirclecastResult_circle}
	 * @param {Ray} ray
	 * @param {Vec2} circlePos 
	 * @param {Number} t1 
	 * @param {Number} t2 
	 * @param {Number} rayWidth 
	 */
	constructor(ray, rayWidth, circlePos, t1, t2){
		super(ray);

		/** @type {Vec2} */
		this.circlePos = circlePos;
		/** @type {Number} */
		this.t1 = t1;
		/** @type {Number} */
		this.t2 = t2;
		/** @type {Number} */
		this.rayWidth = rayWidth;
		
		// private values will be calculated only when/if their corresponding properties 
		// are requested
		/** @private @type {Vec2} */
		this._entryNormal = null;
		/** @private @type {Vec2} */
		this._exitNormal = null;
		/** @private @type {Vec2} */
		this._entryPoint = null;
		/** @private @type {Vec2} */
		this._exitPoint = null;
		/** @private @type {Vec2} */
		this._firstContact = null;
		/** @private @type {Vec2} */
		this._lastContact = null;
	}
	
	/** @override @type {Boolean} */
	get didEnter(){
		return this.isValid && 0.0 <= this.t1 && this.t1 <= 1.0;
	}
	/** @override @type {boolean} */
	get didExit(){
		return this.isValid && 0.0 <= this.t2 && this.t2 <= 1.0;
	}
	
	/** @override @type {Vec2} */
	get entryNormal(){
		if(this._entryNormal == null)
			this._entryNormal = this.circlePos.minus(
				Vec2.Lerp(this.ray.rayStart, this.ray.rayEnd, this.t1)
				).normalized.inverted;
				
		return this._entryNormal;
	}
	/** @override @type {Vec2} */
	get exitNormal(){
		if(this._exitNormal == null)
			this._exitNormal = this.circlePos.minus(
				Vec2.Lerp(this.ray.rayStart, this.ray.rayEnd, this.t2)
				).normalized.inverted;

		return this._exitNormal;
	}
	
	/** @override @type {Vec2} */
	get entryPoint(){
		if(this._entryPoint == null)
			this._entryPoint = Vec2.Lerp(this.ray.rayStart, this.ray.rayEnd, this.t1);
		return this._entryPoint;
	}
	/** @override @type {Vec2} */
	get exitPoint(){
		if(this._exitPoint == null)
			this._exitPoint = Vec2.Lerp(this.ray.rayStart, this.ray.rayEnd, this.t2);
		return this._exitPoint;
	}
	
	/** @override @type {Vec2} */
	get firstContact(){
		if(this._firstContact == null)
			this._firstContact = this.entryPoint.plus(this.entryNormal.scale(-circlecastWidth));
		return this._firstContact;
	}
	/** @override @type {Vec2} */
	get lastContact(){
		if(this._lastContact == null)
			this._lastContact = this.exitPoint.plus(this.exitNormal.scale(-circlecastWidth));
		return this._lastContact;
	}
}